
import java.io.IOException;


public class FindPrimes {
    
    public static void main(String[] args) throws IOException{
        int number = Integer.parseInt(args[0]);
        for (int i = 2;i<number;i++) {
            if(i == 2){
                System.out.print(i);
            }else if(i == 3 || i==5 || i==7){
                System.out.print(", "+i);
            }else if (i%2 ==0 || i%3 == 0 || i%5 == 0 || i%7 == 0){
                continue;
            }else{
                System.out.print(", "+i);


            }
        }


    }
}