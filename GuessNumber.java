import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
		int number =rand.nextInt(100); //generates a number between 0 and 99
		int attempts = 0;
		
		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
		System.out.print("Can you guess it: ");
		
                while (true) {
                    int guess = reader.nextInt(); //Read the user input
                    if (guess == -1) {
                        break;
                    } else if (guess == number) {
                        System.out.println("Congratulations! You won after " + attempts + " attempts!");
                        break;
                    } else if (guess > number) {
                        System.out.println("Sorry!\nMine ise less than your guess.");
                        System.out.print("Type -1 to quit or guess another: ");
                    } else {
                        System.out.println("Sorry!\nMine ise greater than your guess.");
                        System.out.print("Type -1 to quit or guess another: ");
                    }

                    attempts++;
                }
		
		
		reader.close(); //Close the resource before exiting
	

                
	
	

        }
}